const express = require('express')
const cors = require('cors')
const app = express()

app.use(express.static('public'))
app.use(cors())

app.get('/', (req, res) => {
  const i = randomInt(9)
  res.send(JSON.stringify({
    image: `/cat-${i}.jpeg`,
    name: catNames(i),
    breed: catBreads(i)
  }))
})

app.listen(8000, () => {
  console.log('Example app listening on port 8000!')
})

function catNames(i){
  return [ 'Flufy', 'Daeeny', 'Miau', 'Madona', 'Betina', 'Diana', 'Betina', 'Margarida', 'Matias'][i]
}

function catBreads(i){
  return ['Persian','Siamês','Himalaia','Angorá','Siberian','Sphynx','Burmese','Ragdoll','British Shorthair'][i]
}

function randomInt(max){
  return parseInt(Math.random()*max, 10) + 1
}
