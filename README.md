Just a random cats api, to play

# Install
```bash
git clone https://gitlab.com/AndreAntunesVieira/random-cats-open-api
cd random-cats-open-api
yarn install
```

# Run
```bash
yarn start
```

Will open a server on port 8000
